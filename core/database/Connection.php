<?php

class Connection
{

    public static function make($config)
    {
        try {
            //return new PDO('mysql:host=localhost;dbname=mytodo', 'root', '');

            return new PDO(
                $config['connection'] . ';' . $config['dbname'],
                $config['username'],
                $config['password'],
                $config['options']
            );
        } catch (PDOException $error) {
            die($error->getMessage());
        }
    }
}
