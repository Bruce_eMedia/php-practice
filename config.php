<?php

return [
    'database' => [
        'connection' => 'mysql:host=localhost',
        'dbname' => 'dbname=mytodo',
        'username' => 'root',
        'password' => '',
        'options' => [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]
    ]
];
